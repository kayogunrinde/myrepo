from django.apps import AppConfig


class KayogunrindescrumyConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'kayogunrindescrumy'
