from django.shortcuts import render
from .models import ScrumyGoals, ScrumyHistory, GoalStatus




# Create your views here.
from django.http import HttpResponse

def index(request):
	scrumy_goal = ScrumyGoals.objects.get(goal_name='Learn Django')
	return HttpResponse(scrumy_goal.goal_name)


def move_goal(request, goal_id):
	goal = ScrumyGoals.objects.get(goal_id=goal_id)
	return HttpResponse(goal.goal_name)